#ifndef TEMPLATE_ROUTINE_TEMPLATE_ROUTINE_HPP
#define TEMPLATE_ROUTINE_TEMPLATE_ROUTINE_HPP

#include <rip/routine.hpp>

namespace rip::routines 
{

    class TemplateRoutine : public Routine
    {
        public:

            TemplateRoutine(const nlohmann::json &config, std::shared_ptr<EventSystem> es, 
                    std::string id, CompTable comps);

            virtual void start(std::vector<std::any> data = {}) override;

            virtual void stop(std::vector<std::any> data = {}) override;

        protected:

            virtual void saveComponents(CompTable comps) override;

            virtual void run() override;
    };

}

#endif // TEMPLATE_ROUTINE_TEMPLATE_ROUTINE_HPP

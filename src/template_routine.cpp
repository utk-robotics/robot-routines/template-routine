#include <rip/routines/template_routine.hpp>

namespace rip::routines
{
    
    TemplateRoutine::TemplateRoutine(const nlohmann::json &config, std::shared_ptr<EventSystem> es, 
            std::string id, CompTable comps)
    : Routine(config, es, id, comps)
    {}

    void TemplateRoutine::start(std::vector<std::any> data)
    {}

    void TemplateRoutine::stop(std::vector<std::any> data)
    {}

    void TemplateRoutine::saveComponents(CompTable comps)
    {}

    void TemplateRoutine::run()
    {}

}
